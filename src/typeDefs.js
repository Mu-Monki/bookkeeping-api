import { gql } from 'apollo-server';

export const typeDefs = gql`
    type User {
        id: ID!
        firstName: String!
        lastName: String!
        email: String!
        token: String
    }

    type Book {
        id: ID!
        title: String!
        author: String!
        price: Float!
        stocks: Int!
    }

    type Query {
        user(id: ID!): User!
        users: [User]!
        book(id: String!): Book
        books: [Book]!
        me: User
    }

    type AuthPayload {
        token: String!
        user: User!
    }

    input UserInput {
        firstName: String!
        lastName: String!
        email: String!
        password: String! 
    }

    input BookInput {
        title: String!
        author: String!
        price: Float!
        stocks: Int!
    }

    type Mutation {
        login(email: String!, password: String!): AuthPayload!
        addUser(data: UserInput!): AuthPayload!
        updateUser(id: ID!, data: UserInput): User!
        deleteUser(id: ID!): User!
        addBook(data: BookInput!): Book!
        updateBook(id: ID!, data: BookInput): Book!
        deleteBook(id: ID!): Book!
    }
`;