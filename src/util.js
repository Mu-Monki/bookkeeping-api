import jwt from 'jsonwebtoken';
import { constants } from './constants';

export const getClaims = (request) => {
    const authorization = request.headers.authorization;

    if(!authorization) {
        throw new Error('Authentication Required');
    } 

    const token = authorization.replace('Bearer ', '');
    const claims = jwt.verify(token, constants.JWT_SECRET);

    return claims;
}