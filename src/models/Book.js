import mongoose from 'mongoose';

export const Book = mongoose.model("Book", {
    title: String,
    author: String,
    price: Number,
    stocks: Number,
});