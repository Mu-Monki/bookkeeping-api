import { v4 as uuidv4 } from 'uuid';
import { User } from './models/User';
import { db } from './db';
import { Book } from './models/Book';
import { constants } from './constants';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { getClaims } from './util';

const token = jwt.sign({ id: 4 }, constants.JWT_SECRET );

export const resolvers = {
    Query: {
        async user(parent, args, ctx, info) {
            console.log('finding', args.id);
            const user = await User.findById(args.id);

            console.log('user', user);
            return user;
        },

        users(parent, args, ctx, info) {
            return User.find();
        },

        async book(parent, args, ctx, info) {
            const claims = getClaims(ctx.req);

            console.log('token', claims);

            console.log('finding', args.id);
            const book = await Book.findById(args.id);

            console.log('book', book);
            return book;
        },
        books(parent, args, ctx, info) {
            const claims = getClaims(ctx.req);

            console.log('token', claims);
            return Book.find();
        }
    },

    Mutation: {
        async login(parent, args, ctx, info) {
            console.log('args', args.email);
            console.log('args', args.password);
            const email = args.email;
            const password = args.password;

            const user = await User.findOne({ email: args.email });
            const isMatch = await bcrypt.compare(password, user.password);

            if(!(user && isMatch)) {
                throw new Error('Email and Password not valid');
            }

            jwt.sign({ userId: user.id }, constants.JWT_SECRET);

            return {
                user,
                token
            };
        },

        async addUser(parent, args, ctx) {

            if(args.data.password.length <= 8) {
                throw new Error('Password must be 8 characters or longer');
            }

            const hash = await bcrypt.hash(args.data.password, 10);
            const user = new User({
                firstName: args.data.firstName,
                lastName: args.data.lastName,
                email: args.data.email,
                password: hash,
            });

            await user.save().then(() => {
                console.log('user is saved', user);
            });

            const token = jwt.sign({
                userId: user._id
            }, constants.JWT_SECRET);

            console.log(jwt.decode(token));

            return { 
                user,
                token
            };
        },

        async deleteUser(parent, args, ctx, info) {
            const user = await User.findById(args.id);
            await User.findByIdAndDelete(args.id, 
                (err, data) => {
                    if(err) {
                        console.log('error', err);
                    } else {
                        console.log('deleted', data);
                    }
                });

            return user;
        },

        async addBook(parent, args, ctx, info) {
            const claims = getClaims(ctx.req);

            console.log('token', claims);

            const book = new Book({
                title: args.data.title,
                author: args.data.author,
                price: args.data.price,
                stocks: args.data.stocks,
            });

            await book.save().then(() => {
                console.log('book is saved', book);
            });

            return book;
        },

        async updateBook(parent, args, ctx, info) {
            const claims = getClaims(ctx.req);

            console.log('token', claims);

            const book = await Book.findByIdAndUpdate(
                args.id,
                {
                    title: args.data.title,
                    author: args.data.author,
                    price: args.data.price,
                    stocks: args.data.stocks
                }, 
                {
                    new: true
                },
                (err, data) => {
                    if(err) {
                        console.log('error', err);
                    }
                    console.log('data updated', data);
                }
            );

            return book;
        },

        async deleteBook(parent, args, ctx, info) {
            const claims = getClaims(ctx.req);

            console.log('token', claims);

            const book = await Book.findById(args.id);
            await Book.findByIdAndDelete(args.id, 
                (err, data) => {
                    if(err) {
                        console.log('error', err);
                    } else {
                        console.log('deleted', data);
                    }
                });

            return book;
        }
    }
}