export const db = {
    users: [
        {
            id: "0",
            firstName: "Francis",
            lastName: "Garcia",
            token: "",
        },
        {
            id: "1",
            firstName: "Miguel",
            lastName: "Go",
            token: "",
        },
    ],

    books: [
        {
            id: "0",
            title: "Book 1",
            author: "Mark Twain",
            price: 500.00,
            stocks: 7,
        },
        {
            id: "1",
            title: "Book 2",
            author: "Antoine Saint Exupery",
            price: 500.00,
            stocks: 7,
        }
    ]
};