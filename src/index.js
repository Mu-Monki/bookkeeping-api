import { ApolloServer, gql } from 'apollo-server';
import express from 'express';
import mongoose from 'mongoose';

import { typeDefs } from './typeDefs';
import { resolvers } from './resolvers';

require('dotenv').config();

const startServer = async () => {
    const app = express();
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context({ req }) { 
            return {
                req
            }
        }
    });

    await mongoose.connect(process.env.DATABASE_URL, { useFindAndModify: false, useNewUrlParser: true, useUnifiedTopology: true });
    // server.applyMiddleware({ app });

    server.listen({port: process.env.PORT}).then(({ url }) => {
        console.log(`server started at: ${ url }`);
    });
}

startServer();